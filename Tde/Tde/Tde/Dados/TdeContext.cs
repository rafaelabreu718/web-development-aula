﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tde.Models;


namespace Tde.Dados
{
    public class TdeContext : DbContext
    {

        public TdeContext(DbContextOptions<TdeContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FuncionarioDepartamento>()
                 .HasKey(ac => new { ac.FuncionarioId, ac.DepartamentoId });
        }

        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Gerente> Gerentes { get; set; }
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<FuncionarioDepartamento> FuncionarioDepartamentos{ get; set; }


    }
}
