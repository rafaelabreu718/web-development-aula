﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tde.Models
{
    public class Departamento
    {
        public int Id { get; set; }
        public string NomeDepartamento { get; set; }
        public DateTime DataDeInicio { get; set; }
        public int GerenteID { get; set; }
        public List<FuncionarioDepartamento> funcionarioDepartamentos { get; set; }
        public Gerente gerente{ get; set; }
    }
}
