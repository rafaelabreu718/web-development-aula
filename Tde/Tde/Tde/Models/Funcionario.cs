﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tde.Models
{
    public class Funcionario
    {
        public int id { get; set; }
        public string Nome{ get; set; }
        public string Email { get; set; }
        public List<FuncionarioDepartamento> funcionarioDepartamentos { get; set; }
    }
}
