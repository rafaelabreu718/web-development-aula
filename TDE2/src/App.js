import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import { Navbar, NavItem, NavDropdown, MenuItem, Nav } from 'react-bootstrap';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { Home } from './components/Home';
import {Funcionarios} from './components/Funcionarios';
import {Departamentos} from './components/Departamentos';

function App() {
  return (
    <Container className="Container">
      <BrowserRouter>
        <Navbar bg='light' expand='lg'>
          <Navbar.Brand as={Link} to="/">Empresa X</Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='mr-auto'>
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <NavDropdown title='Cadastro' id='basic-nav-dropdown'>
                <NavDropdown.Item as={Link} to='/funcionarios'>Funcionarios</NavDropdown.Item>
                <NavDropdown.Item as={Link} to='/departamentos'>Departamentos</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/funcionarios" component={Funcionarios} />
          <Route path="/departamentos" component={Departamentos} />
          
        </Switch>
      </BrowserRouter>
    </Container>
  );
}

export default App;
