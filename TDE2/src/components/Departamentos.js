import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './App.css';

export class Departamentos extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      Departamentos: [],
      modalAberta: false
    };

    this.buscarDepartamentos = this.buscarDepartamentos.bind(this);
    this.buscarDepartamento = this.buscarDepartamento.bind(this);
    this.inserirDepartamento = this.inserirDepartamento.bind(this);
    this.atualizarDepartamento = this.atualizarDepartamento.bind(this);
    this.excluirDepartamento = this.excluirDepartamento.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarDepartamentos();
  }

  // GET (todos Departamentos)
  buscarDepartamento() {
    fetch('http://localhost:60963/api/departamento')
      .then(response => response.json())
      .then(data => this.setState({ departamentos: data }));
  }
  
  //GET (Departamento com determinado id)
  buscarDepartamento(id) {
    fetch('http://localhost:60963/api/departamentos/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirDepartamento = (departamento) => {
    fetch('http://localhost:60963/api/departamentos', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(departamento)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscardepartamentos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarDepartamento(departamento) {
    fetch('http://localhost:60963/api/departamentos/' + departamento.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(departamento)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarDepartamentos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirDepartamento = (id) => {
    fetch('http://localhost:60963/api/Departamentos/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarDepartamentos();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarDepartamento(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const departamento = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirdepartamento(departamento);
    } else {
      this.atualizarDepartamento(departamento);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do departamento</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do departamento' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type='email' placeholder='Email' value={this.state.email} onChange={this.atualizaEmail} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th> Descrição</th>
            <th>Prioridade</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {this.state.departamentos.map((departamento) => (
            <tr key={departamento.id}>
              <td>{departamento.nome}</td>
              <td>{departamento.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(departamento.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirdepartamento(departamento.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar departamento</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}